<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('employees')->insert([
    		'id' => '1',
    		'name' => 'Allan',
    		'last_name' => 'Poe',
    		'email' => 'allan_poe_09@email.com',
    		'phone' => '4435679083',
    	]);

    	DB::table('employees')->insert([
    		'id' => '2',
    		'name' => 'Clive S.',
    		'last_name' => 'Lewis',
    		'email' => 'cs_lewis_98@email.com',
    		'phone' => '4437094524',
    	]);

    	DB::table('employees')->insert([
    		'id' => '3',
    		'name' => 'JK',
    		'last_name' => 'Rowling',
    		'email' => 'jk_rowling_65@email.com',
    		'phone' => '4436122456',
    	]);

    	DB::table('employees')->insert([
    		'id' => '4',
    		'name' => 'Cassandra',
    		'last_name' => 'Clare',
    		'email' => 'cassandra_clare_73@email.com',
    		'phone' => '4437972345',
    	]);

    	DB::table('employees')->insert([
    		'id' => '5',
    		'name' => 'Stephen',
    		'last_name' => 'King',
    		'email' => 'stephen_king_47@email.com',
    		'phone' => '4438996523',
    	]);
    }
}
