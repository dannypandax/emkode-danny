<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employees extends Model
{
    protected $fillable = [
        'name', 'email', 'password', 'phone'
    ];
}
