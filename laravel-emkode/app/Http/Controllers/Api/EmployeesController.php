<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\employees;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = employees::get();

        if ($employees) {

            return response()->json([
                'success'    => true,
                'response' => $employees,
            ]);
            
        }
        else{
            return response()->json([
                'success'    => false,
                'response' => 'Algo salió mal. Intenta más tarde.',
            ]);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $employee = new employees;

        $employee->name = $request->name;

        $employee->last_name = $request->last_name;

        $employee->email = $request->email;

        $employee->phone = $request->phone;

        $employee->save();




        if ($employee) {

            return response()->json([
                'success'    => true,
                'response' => 'Empleado añadido exitosamente.',
            ]);
            
        }
        else{
            return response()->json([
                'success'    => false,
                'response' => 'Algo salió mal. Intenta más tarde.',
            ]);
        }

    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $employee = employees::find($request->id);

        $employee->name = $request->name;

        $employee->last_name = $request->last_name;

        $employee->email = $request->email;

        $employee->phone = $request->phone;

        $employee->save();

        if ($employee) {

            return response()->json([
                'success'    => true,
                'response' => 'Datos actualizados.',
            ]);
            
        }
        else{
            return response()->json([
                'success'    => false,
                'response' => 'Algo salió mal. Intenta más tarde.',
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $employee = employees::find($request->id);

        $employee->delete();

         if ($employee) {

            return response()->json([
                'success'    => true,
                'response' => 'Empleado eliminado.',
            ]);
            
        }
        else{
            return response()->json([
                'success'    => false,
                'response' => 'Algo salió mal. Intenta más tarde.',
            ]);
        }
    }

    public function search(Request $request)
    {
        $employee = employees::where('name', 'like', '%' . $request->buscar . '%')
        ->orWhere('last_name', 'like', '%' . $request->buscar . '%')
        ->orWhere('email', 'like', '%' . $request->buscar . '%')
        ->orWhere('phone', 'like', '%' . $request->buscar . '%')
        ->orWhere('id', 'like', '%' . $request->buscar . '%')
        ->get();

         if ($employee) {

            return response()->json([
                'success'    => true,
                'response' => $employee,
            ]);
            
        }
        else{
            return response()->json([
                'success'    => false,
                'response' => 'Algo salió mal. Intenta más tarde.',
            ]);
        }
    }
}
