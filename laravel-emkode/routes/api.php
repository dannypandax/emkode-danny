<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('employees/index', 'Api\EmployeesController@index')->name('index');

Route::post('employee/store', 'Api\EmployeesController@store')->name('store');

Route::post('employee/update', 'Api\EmployeesController@update')->name('update');

Route::post('employee/destroy', 'Api\EmployeesController@destroy')->name('destroy');

Route::post('employee/search', 'Api\EmployeesController@search')->name('search');
